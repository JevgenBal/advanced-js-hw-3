const user1 = {
  name: "John",
  years: 30
};

const { name: name, years: вік, isAdmin = false } = user1;

console.log(name);
console.log(вік);
console.log(isAdmin);
